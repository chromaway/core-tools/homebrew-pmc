# Homebrew Chromia

Welcome to the Homebrew Chromia repository, your source for distributing Chromia CLI (CHR), Postchain Management Console (PMC), and Chromia Server through the Homebrew package manager.

## New Release

To create a new release, follow these steps:

1. **Trigger a New Pipeline on GitLab**

    - Go to the [GitLab Pipeline](https://gitlab.com/chromaway/core-tools/homebrew-chromia/-/pipeline) for this repository.
    - Click on "Run Pipeline."

2. **Provide the Required Parameters**

   When triggering the pipeline manually, ensure you provide the following parameters:

    - `URL`: The URL pointing to the corresponding tar file of the release.
    - `VERSION`: The new version number.
    - `TOOL`: The name of the tool (chr / pmc / chromia-server).

3. **Run the Pipeline**

   After adding the parameters, click "Run Pipeline" to initiate the release process.

This process will help you create and distribute new releases of Chromia CLI and Postchain Management Console using Homebrew. If you have any questions or encounter issues, please don't hesitate to reach out for assistance.
