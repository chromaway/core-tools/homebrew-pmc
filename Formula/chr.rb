class Chr < Formula
  desc "Chromia Cli"
  homepage "https://docs.chromia.com"
  url "https://gitlab.com/api/v4/projects/39844192/packages/maven/com/chromia/cli/chromia-cli/0.24.2/chromia-cli-0.24.2-dist.tar.gz"
  version "0.24.2"
  sha256 "9f2d1321d29af83bc4f7bd974ae774db3a8c5c39a5b950e976f3b9c33eb5b34b"
  license ""

  depends_on "openjdk@21"

  def install
    libexec.install "lib"
    libexec.install "bin"
    Pathname.glob("#{libexec}/bin/*") do |file|
      next if file.directory?

      basename = file.basename

      (bin/basename).write_env_script file, Language::Java.overridable_java_home_env
    end
  end
  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test pmc`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "#{bin}/chr", "--version"
  end
end
