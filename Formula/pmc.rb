class Pmc < Formula
  desc "Postchain Management Console"
  homepage "docs.chromia.com"
  url "https://gitlab.com/api/v4/projects/46346037/packages/maven/net/postchain/mc/management-console/3.43.0/management-console-3.43.0-dist.tar.gz"
  version "3.43.0"
  sha256 "db1f96cdcae026b91fbf0350a3ee64d96b5671dba8880f75c7bf056cca28fb55"
  license ""

  depends_on "openjdk@21"

  def install
    libexec.install "lib"
    libexec.install "bin"
    Pathname.glob("#{libexec}/bin/*") do |file|
      next if file.directory?

      basename = file.basename

      (bin/basename).write_env_script file, Language::Java.overridable_java_home_env
    end
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test pmc`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "#{bin}/pmc", "--help"
  end
end
