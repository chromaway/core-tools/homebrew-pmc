class ChrAT017 < Formula
  desc "Chromia Cli"
  homepage "https://docs.chromia.com"
  url "https://gitlab.com/api/v4/projects/39844192/packages/maven/com/chromia/cli/chromia-cli/0.17.4/chromia-cli-0.17.4-dist.tar.gz"
  version "0.17.4"
  sha256 "20db4a7dbfd7db2ce4c01611121b0f50abf5c2e407c3b32e966427bea3a55c86"
  license ""

  depends_on "openjdk@17"
  keg_only :versioned_formula

  def install
    libexec.install "lib"
    libexec.install "bin"
    Pathname.glob("#{libexec}/bin/*") do |file|
      next if file.directory?

      basename = file.basename

      (bin/basename).write_env_script file, Language::Java.overridable_java_home_env
    end
  end
  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test pmc`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "#{bin}/chr", "--version"
  end
end
