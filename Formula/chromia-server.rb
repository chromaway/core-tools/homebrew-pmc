class ChromiaServer < Formula
  desc "Chromia Server"
  homepage "https://docs.chromia.com"
  url "https://gitlab.com/chromaway/postchain-chromia/-/package_files/83055205/download"
  version "3.11.2"
  sha256 "56f09e145643a04f9f8a849ba80dfeba5d5dfbbe08d355f7aa91cc416acedebc"
  license ""

  depends_on "openjdk@17"
  depends_on "postgresql@14"

  def install
    libexec.install "lib"
    libexec.install "bin"
    libexec.install "config"
    inreplace Pathname.glob("#{libexec}/config/log4j2.yml"), "value: \"logs\"", "value: \"#{var}/log/chromia\""
    Pathname.glob("#{libexec}/bin/chromia.sh") do |file|
      next if file.directory?

      basename = "chromia-server"

      (bin/basename).write_env_script file, Language::Java.overridable_java_home_env
    end
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test pmc`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "#{bin}/chromia-server", "--help"
  end
end
